# Magento 2 SimplyScroll

Ten prosty moduł pozwala na korzystanie z simplyScroll zgodnie ze sposobem Magento.

## How to install

**Ten moduł jest teraz dostępny za pośrednictwem * Packagist *! Nie musisz już określać repozytorium.**

Dodaj następujące wiersze do pliku composer.json

```
...
"require":{
    ...
    "kowal/module-simplyscroll":"^1.0.0"
 }
```
lub po prostu wykonaj polecenie w konsoli
```
composer require kowal/module-simplyscroll
```

Następnie wpisz następujące polecenia z katalogu głównego Magento:

```
$ composer update
$ ./bin/magento cache:disable
$ ./bin/magento module:enable Kowal_OwlCarousel
$ ./bin/magento setup:upgrade
$ ./bin/magento cache:enable
```

## Jak używać

Stosując `data-mage-init`:

 ```html
 
 <div id="kowal-simply-scroll" data-mage-init='{
   "simplyScroll":{
         "autoPlay": 3000,
         "items" : 5,
         "itemsDesktop" : [1290,3],
         "itemsDesktopSmall" : [768,3]
   }
 }
 '>
     <div class="item"><img src="/image1.jpg" alt="Image"></div>
     <div class="item"><img src="/image2.jpg" alt="Image"></div>
     <div class="item"><img src="/image3.jpg" alt="Image"></div>
     <div class="item"><img src="/image4.jpg" alt="Image"></div>
     <div class="item"><img src="/image5.jpg" alt="Image"></div>
     <div class="item"><img src="/image6.jpg" alt="Image"></div>
 </div>
 ```

Stosując tag `<script>`:

 ```html
 <div id="kowal-simply-scroll">
     <div class="item"><img src="/image1.jpg" alt="Image"></div>
     <div class="item"><img src="/image2.jpg" alt="Image"></div>
     <div class="item"><img src="/image3.jpg" alt="Image"></div>
     <div class="item"><img src="/image4.jpg" alt="Image"></div>
     <div class="item"><img src="/image5.jpg" alt="Image"></div>
     <div class="item"><img src="/image6.jpg" alt="Image"></div>
 </div>
 
 <script type="text/x-magento-init">
     {
         "#kowal-simply-scroll": {
             "SimplyScroll": {
                 "speed": 1
             }
         }
     }
 </script>
 ```

Basic usage (CommonJS)
 ```html
<script>
        var $ = require('jquery');
require('jquery-simplyscroll');

$("#scroller").simplyScroll({
speed: 1
});
</script>
 ```
CONFIGURATION
simplyScroll can be configured with a number of options
```
Property	Default	Description
customClass	'simply-scroll'	Class name for styling
frameRate	24	Number of movements/frames per second
speed	1	Number of pixels moved per frame, in 'loop' mode must be divisible by total width of scroller
orientation	'horizontal'	'horizontal or 'vertical' - not to be confused with device orientation
direction	'forwards'	'forwards' or 'backwards'
auto	true	Automatic scrolling, use false for button controls
autoMode	'loop'	auto = true, 'loop' or 'bounce', (disables buttons)
manualMode	'end'	auto = false, 'loop' or 'end' (end-to-end)
pauseOnHover	true	Pause scroll on hover (auto only)
pauseOnTouch	true	Touch enabled devices only (auto only)
pauseButton	false	Creates a pause button (auto only)
startOnLoad	false	Init plugin on window.load (to allow for image loading etc)
initialOffset	0	Start with an initial pixel offset
```


